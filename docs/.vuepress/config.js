module.exports = {
    lang: 'en-US',
    title: 'Baptiste Dauphin\'s doc',
    description: 'Welcome home',
    head: [['link', { rel: 'icon', href: '/otter.png' }]],
    dest: 'public',
    themeConfig: {
    	logo: 'https://vuejs.org/images/logo.png',
    	repo: 'https://gitlab.com/baptiste-dauphin/doc',
    	lastUpdated: 'true',
    	docsBranch: 'master',
    	docsDir: 'docs',
    	search: true,
    	searchMaxSuggestions: 10,
    	navbar: [
  			{
			    text: 'Technologies',
			    children: [
			      {
			        text: 'Common',
			        children: [
			          '/common/database.md',
			          '/common/dns.md',
			          '/common/gitlab.md',
			          '/common/git.md',
			          '/common/mail.md',
			          '/common/monitoring.md',
			          '/common/network.md',
			          '/common/provisioning.md',
			          '/common/security.md',
			          '/common/software.md',
			          '/common/storage.md',
			          '/common/system.md',
			          '/common/virtualization.md',
			          '/common/web_infra.md',
			        ],
			      },
			      {
			        text: 'Container',
			        children: [
			          '/container/docker.md',
			          '/container/docker-swarm.md',
			          '/container/kubernetes.md'
			        ],
			      },
			      {
			        text: 'Language',
			        children: [
			          '/language/python.md',
			          '/language/php.md',
			          '/language/golang.md',
			          '/language/c.md',
			          '/language/json.md',
			          '/language/markdown.md'
			        ],
			      },
			      {
			        text: 'Os',
			        children: [
			          '/os/archlinux.md',
			          '/os/centos.md',
			          '/os/debian.md',
			        ],
			      },
			      {
			        text: 'Cloud',
			        children: [
					  '/cloud/dns-provider.md',
			        ],
			      },
    			]
    		},
  			{
			    text: 'Misc',
			    children: [
			      {
			        text: 'Miscellaneous',
			        children: [
			          '/misc/definitions.md',
			          '/misc/desktop.md',
			          '/misc/dev-friendly.md',
			          '/misc/hardware.md',
			          '/misc/ldap.md',
			          '/misc/media_platform.md',
			          '/misc/microsoft.md',
			          '/misc/miscellaneous.md',
			          '/misc/naming_convention.md',
			          '/misc/organization.md',
			          '/misc/raspberry.md',
			          '/misc/regex.md',
			          '/misc/terminal.md',
			          '/misc/text-editor.md',
			          '/misc/blog-roll.md',
			    	],
				  },
    			]
    		},
    		{
		    	text: 'Contact',
    			link: '/contact/',
  			},
    	]
  	},
  	plugins: [
    [
      '@vuepress/plugin-docsearch',
      {
        apiKey: 'a6844929e502d102038724edb2296172',
        indexName: 'doc',
        locales: {
          '/': {
            placeholder: 'Search Documentation',
          }
        },
      },
    ],
  ],
}
