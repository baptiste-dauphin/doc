# DNS
The Domain Name System is a `hierarchical` and `decentralized` __naming system__ for computers, services, or other resources connected to the Internet or a private network.  
It associates various information with domain names assigned to each of the participating entities.  

<img :src="$withBase('/src/Domain_name_space.svg')" alt="domain name space">

## Two types of role
### 1 - Authoritative
Authoritative means the server is responsible for a (or several) zone(s). It is `the only one` (master + slaves (if exists)) which contains the records of this zone ! It's a part of the global dns database.

Example of technologies which support authoritative role `by design` (*not by using plugin, config trick or whatever*) : 
- [Bind](https://en.wikipedia.org/wiki/BIND) by [ISC](https://www.isc.org/bind/)
- [NSD](https://en.wikipedia.org/wiki/NSD) by [NL netlabs](https://www.nlnetlabs.nl/projects/nsd/about/)
- [PowerDNS Recursor](https://en.wikipedia.org/wiki/PowerDNS) by [OX](https://www.open-xchange.com/)
- [Knot](https://en.wikipedia.org/wiki/Knot_DNS) by [CZ.NIC](https://www.knot-dns.cz/)

### 2 - Recusrive
Example of technologies which support recursive role `by design` (*not by using plugin, config trick or whatever*) :
- [Bind](https://en.wikipedia.org/wiki/BIND) by [ISC](https://www.isc.org/bind/)
- [Unbound](https://en.wikipedia.org/wiki/Unbound_(DNS_server) by [NL netlabs](https://www.nlnetlabs.nl/projects/unbound/about/)
- [PowerDNS Recursor](https://en.wikipedia.org/wiki/PowerDNS) by [OX](https://www.open-xchange.com/)


## Implementation
### Bind9
Bind is both an `authoritative AND recursive` domain main server

https://wiki.csnu.org/index.php/Installation_et_configuration_de_bind9  
the process name of bind9 is "named"

#### rndc (bind runtime management tool)
__name server control utility__

Write (dump) cache of named in default file (__/var/cache/bind/named_dump.db__)  
dumpdb [-all|-cache|-zones|-adb|-bad|-fail] [view ...]
```bash
rndc dumpdb -cache default_any
```

enable query logging in default location (__/var/log/bind9/query.log__)
```bash
rndc querylog [on|off]
```

toggle querylog mode
```bash
rndc querylog
```

flush   Flushes all of the server's caches.
```bash
rndc flush
```
flush [view]  Flushes the server's cache for a view.
```bash
rndc flush default_any
```

get unic master zone loaded
```bash
named-checkconf -z 2> /dev/null | grep 'zone' | sort -u | awk '{print $2}' | rev | cut --delimiter=/ -f2 | rev | sort -u
named-checkconf -z 2> /dev/null | grep 'zone' | grep -v 'bad\|errors' | sort -u | awk '{print $2}' | rev | cut --delimiter=/ -f2 | rev | sort -u
```

keep cache
```bash
systemctl reload bind9
```
empty cache
```bash
systemctl restart bind9
```

### Knot
#### Troubleshooting command
Manually trigger a zone transfer from master to slave.  
To run on __slave__ side
```bash
knotc zone-retransfer example.org
```

Reload a zone from a disk and send NOTIFY to slave.  
To run on __master__ side
```bash
knotc zone-reload example.org
```

### NSD
I advice you to check the fellow's tutorial

https://toutetrien.lithio.fr/article/installer-son-serveur-dns-faisant-autorite-avec-nsd

## Record types
### CAA
CAA stands for `Certificate Authority Authorization`  
I.e. : Which CA do you allow to deliver certificate for your domain ?  
How to generate your CAA record, for both wildcard + non-wildcard certificate common name.  
https://sslmate.com/caa/

## Query a dns server
dig
```bash
dig @8.8.8.8 +short +notcp  google.com 
dig @8.8.8.8 +noall +answer +tcp example.org A
dig @8.8.8.8 +noall +answer +notcp example.org A

dig @8.8.8.8 google.com +short +subnet=192.168.1.0/24
dig @8.8.8.8 google.com +short +subnet=10.0.0.0/8

dig @8.8.8.8 +short example.org +nodnssec
```

others options
- +short
- +(no)tcp
- +(no)dnssec
- +noall
- +answer
- type

[Full manual](https://linux.die.net/man/1/dig)

## DNSSEC
- Only one key is known and trusted by all resolver of the world. (The one of the `.`)
- We have to think with `zones`

Each zone contains : 
- KSK : Key signing Key
- ZSK : Zone signing Key (signed by KSK)
- DS : (record DNS) Delegation Signer
Each KSK digest of a given zone is contained in the parent zone

Si DNSSEC non activé, alors record NSEC3 ajouté (en auto souvent) dans la zone parente. (Preuve de non existence de la signature du domaine, pour ne pas faire fail le DNSSEC) 

### SoftHSM
softhsm : s'occupe de la gestion des clefs (stockage, generation, signature des ZSK par la KSK)

Exemple : https://dnsviz.net/d/lithio.fr/dnssec/


```bash
ods-enforcer key list --verbose --all --zone lithio.fr
Keys:
Zone:                           Keytype: State:    Date of next transition: Size: Algorithm: CKA_ID:                          Repository: KeyTag:
lithio.fr                       KSK      active    2020-04-24 05:26:19      2048  8          f88125e010c6c1ad671df83048f877f8 SoftHSM     65035
lithio.fr                       ZSK      retire    2020-04-24 05:26:19      1024  8          06ecab532b0b27c8ef2f17d6624c51d1 SoftHSM     65121
lithio.fr                       ZSK      ready     2020-04-24 05:26:19      1024  8          89087g092480f25470ce515c780ca373 SoftHSM     8685
```

## Public Resolver classic flow

<img :src="$withBase('/src/quad9_infographic@2x.png')" alt="flow image">

## Stub zone
A stub zone can ONLY be configured on a `recursive` server. This mecanism is intended to boost zone delegation, by avoiding to start again recursion process from `root servers`.  
If you know the `authoritative nameserver` for a given zone you can avoid recursion process (because you already known, no need to discover).

For `unbound.conf` you can setup something like this :
```
stub-zone:
	name: "dyn.example.com"
	stub-addr: 10.10.10.10,10.10.10.11
```

## Zone delegation
To be updated...

## Performance test

DNS queryperf, un programme de mesure de performance, qui est distribué avec BIND (dans le répertoire contrib/).

Sur une Debian, queryperf peut s'installer ainsi :

### Build queryperf tool

```bash
cd /tmp
apt-get source bind9
cd bind9-9.4.2/contrib/queryperf
./configure
make
# Pas de cible install dans le Makefile
sudo cp queryperf /usr/local/sbin
```
Nous pouvons maintenant le lancer pour voir les options (je n'ai pas tout montré) :

```bash
Usage: queryperf [-d datafile] [-s server_addr] [-p port] [-q num_queries]
                 [-b bufsize] [-t timeout] [-n] [-l limit] [-f family] [-1]
                 [-i interval] [-r arraysize] [-u unit] [-H histfile]
                 [-T qps] [-e] [-D] [-c] [-v] [-h]
  -d specifies the input data file (default: stdin)
  -s sets the server to query (default: 127.0.0.1)
  -q specifies the maximum number of queries outstanding (default: 20)
  -t specifies the timeout for query completion in seconds (default: 5)
```
### Create your record list
In order to have an input file to give to `queryperf`  
Form du fichier 
```
www.0fjtvn3q0i.example     A
www.ngw7m3vq71.example     A
www.2u1.example     A
```

### Run

```bash
./queryperf -d example.com.list -s 10.10.10.10
```

Sources : [Stephane Bortzmeyer](https://www.bortzmeyer.org/performances-serveur-dns.html)

## Security
Web script injection via DNS

```bash
dig avalz.it TXT +short
"<script>alert(\"hi!\")</script>"
```

Try by yourself on weak website : https://dnslookup.online/

## Root servers
https://root-servers.org/index.html

## Name convention proposal
### Private zone
`node`.`fonction`.`env`.`tld`  

ceph case  
`gw1.s3.prod.example.com`  
`gw2.s3.prod.example.com`  
`gw.s3.prod.example.com`  
`s3.prod.example.com CNAME gw.s3.prod.example.com`  

`mon1.s3.prod.example.com`  
`mon2.s3.prod.example.com`  
`mon3.s3.prod.example.com`  

`gw1.s3.staging.example.com`  
`gw2.s3.staging.example.com`  
`mon1.s3.staging.example.com`  
`mon2.s3.staging.example.com`  
`mon3.s3.staging.example.com`  

`gw1.s3.dev.example.com`  
`gw2.s3.dev.example.com`  
`mon1.s3.dev.example.com`  
`mon2.s3.dev.example.com`  
`mon3.s3.dev.example.com`  

MinIO case  
`client.s3.prod.example.com`  
`client.s3.staging.example.com`  
`client.s3.dev.example.com`  

Oio case  
`client.s3.prod.example.com`  
`client.s3.staging.example.com`  
`client.s3.dev.example.com`  

## Resources
https://www.dnsperf.com/dns-providers-list/