# Monitoring
## Zabbix
### Item
#### Item value preprocessing
The Preprocessing tab allows to define transformation rules for the received values. One or several transformations are possible before saving values to the database. Transformations are executed in the order in which they are defined. Preprocessing is done either by Zabbix server or by Zabbix proxy (for items monitored by proxy).

You can
- Matching `Regex` and replace
- `Remove` specified characters from the end of the value.
- Extract value from JSON/XML
- Calculate `difference` between the current and previous value.

<img :src="$withBase('/src/zabbix-item-preprocessing.png')" alt="item preprocessing image">

And [many others](https://www.zabbix.com/documentation/4.2/manual/config/items/item)


### Traper
Test to send to zabbix-server
```bash
zabbix_sender -z <server IP address> -p 10051 -s "New host" -k trap -o "test value"
```


### API usage  
Verify your url
```
https://zabbix.company/zabbix.php?action=dashboard.view
https://zabbix.company/zabbix/zabbix.php?action=dashboard.view
```

### Test zabbix agent key locally
```bash
### Test a given item
zabbix_agentd -t system.hostname
zabbix_agentd -t system.swap.size[all,free]
zabbix_agentd -t vfs.file.md5sum[/etc/passwd]
zabbix_agentd -t vm.memory.size[pavailable]

### print all known items 
zabbix_agentd -p
```

#### zabbix server info request (to perform a first test)
```bash
curl \
-d '{ 
  "jsonrpc":"2.0", 
  "method":"apiinfo.version", 
  "id":1, 
  "auth":null, 
  "params":{} 
  }' \
-H "Content-Type: application/json-rpc" \
-X POST https://zabbix.company/api_jsonrpc.php | jq .
```

##### Authentication request. In order to get TOKEN for the next request
```bash
curl \
-d '{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "b.dauphin",
        "password": "toto"
    },
    "id": 1,
    "auth": null
  }' \
-H "Content-Type: application/json-rpc" \
-X POST https://zabbix.company/api_jsonrpc.php | jq .
```


##### Get all trigger of a specific host
replace $host and $token
```bash
curl \
-d '{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "filter": {
            "host": [
                "$host"
            ]
        },
        "with_triggers": "82567"
    },
    "id": 2,
    "auth": "$token"
  }' \
-H "Content-Type: application/json-rpc" \
-X POST https://zabbix.company/api_jsonrpc.php | jq .
```

##### get hostid with name(s)
Replace `$hostname1`,`$hostname2` and `$token`
```bash
curl \
-d '{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
      "output": ["hostid"],
        "filter": {
            "host": [
                ""$hostname1","$hostname2"
            ]
        }
    },
    "id": 2,
    "auth": "$token"
  }' \
-H "Content-Type: application/json-rpc" \
-X POST https://zabbix.tld/api_jsonrpc.php | jq '.result'
```

##### Get groups of a specific host(s)
Replace `$hostname1`,`$hostname2` and `$token`
```bash
curl \
-d '{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
      "output": ["hostid"],
      "selectGroups": "extend",
        "filter": {
            "host": [
                "$hostname1","$hostname2"
            ]
        }
    },
    "id": 2,
    "auth": "$token"
  }' \
-H "Content-Type: application/json-rpc" \
-X POST https://zabbix.tld/api_jsonrpc.php | jq .
```

##### Get host by TAG(S)
```bash
curl \
-d '{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
      "output": ["name"],     
        "selectTags": "extend",
        "tags": [
            {
                "tag": "environment",
                "value": "dev",
                "operator": 1
            }
        ]
    },
    "id": 2,
    "auth": "$token"
  }' \
-H "Content-Type: application/json-rpc" \
-X POST https://zabbix.company/api_jsonrpc.php | jq .
```

Output __hostid__, __host__ and __name__
```bash
curl \
-d '{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
      "output": ["hostid","host","name"],     
        "tags": [
            {
                "tag": "environment",
                "value": "dev",
                "operator": 1
            }
        ]
    },
    "id": 2,
    "auth": "$token"
  }' \
-H "Content-Type: application/json-rpc" \
-X POST https://zabbix.company/api_jsonrpc.php | jq .
```
##### Get host by multiple TAGS

```bash
curl \
-d '{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
      "output": ["name"],     
        "tags": [
            {
                "tag": "app",
                "value": "swarm",
                "operator": "1"
            },
            {
                "tag": "environment",
                "value": "dev",
                "operator": "1"
            }
        ]
    },
    "id": 2,
    "auth": "$token"
  }' \
-H "Content-Type: application/json-rpc" \
-X POST https://zabbix.tld/api_jsonrpc.php | jq .
```

##### Modify TAG
`Warning` erase all others tags + can set only one tag... So I do not recommend using this shity feature.
```bash
curl \
-d '{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
      "hostid": "12345",
        "tags": [
            {
                "tag": "environment",
                "value": "staging"
            }
        ]
    },
    "id": 2,
    "auth": "$token"
  }' \
-H "Content-Type: application/json-rpc" \
-X POST https://zabbix.tld/api_jsonrpc.php | jq '.result'
```

### Maintenance
https://www.zabbix.com/documentation/4.0/fr/manual/maintenance

## Go access
https://goaccess.io/

Analyze nginx log files to produce dashboard, CLI or Web 
