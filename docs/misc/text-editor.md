# Text editor
## Sublime-text
### Preparing package installation
Before, you can download and install awesome packages, you first have to install the __Package Control package__. It's dumb, but it's not included in sublime-text installation...

The simplest method of installation is through the Sublime Text console. The console is accessed via the __ctrl+\`__ shortcut or the __View > Show Console__ menu. Once open, paste the appropriate Python code for your version of Sublime Text into the console. 

```python
import urllib.request,os,hashlib; h = '6f4c264a24d933ce70df5dedcf1dcaee' + 'ebe013ee18cced0ef93d5f746d80ef60'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)
```
[official source](https://packagecontrol.io/installation)
This code creates the Installed Packages folder for you (if necessary), and then downloads the Package Control.sublime-package into it. The download will be done over HTTP instead of HTTPS due to Python standard library limitations, however the file will be validated using SHA-256.


### Install a package (__insp__)
Open __commande palette__
```
ctrl + shift + p
```
Then, open __sublime-text package manager__, select __Package Control: Install package__ by shortname __insp__
```
insp
```
Then, enter

### My packages

Name | Usage | __insp__ name| URL
----|----|-----|------
Markdown Preview | To see a preview of your README.md files before commit them | `MarkdownPreview` | https://facelessuser.github.io/MarkdownPreview/install/
Compare Side-By-Side | Compares two tabs | `Compare Side-By-Side` | https://packagecontrol.io/packages/Compare%20Side-By-Side
Generic Config | Syntax generic config colorization |
PowerCursors | multiple cursors placements |
Materialize | Several beautiful __color scheme__
MarkdownPreview | Preview your .md file 
MarkdownTOC | Generate your Table of content of MarkDown files

#### MarkdownTOC
To generate a TableOfContent
Insert this piece of code 
```html
<!-- MarkdownTOC levels="1,2" autolink="true" -->

<!-- /MarkdownTOC -->
```
Then, place your cursor in the middle of 2 lines, `ctrl + Maj + p` : `Insert TOC`

### Manuall plugin installation
If you don't find your desired plugin with the package installer. You can right your own code !
```
Tools > Developer > New Plugin...
```

### Absolute cursor position
By default the `status bar` only gives you `Line and Column`, but if you get a json error like `SyntaxError: /builds/baptiste-dauphin/doc/book.json: Unexpected token / in JSON at position 765` it's not fancy.

Paste and save this code
```python
import sublime_plugin


class PositionListener(sublime_plugin.EventListener):

    def on_selection_modified(self, view):
        text = "Position: "
        sels = view.sel()
        for s in sels:
            text += " " + str(s.begin())
            if not s.empty():
                text += "-" + str(s.end()) + " "
        view.set_status('exact_pos', text)
```

*Et voilà*

<img :src="$withBase('/src/sublime-text-cursor-absolute-position.png')" alt="git_cheat">

[source](https://gist.githubusercontent.com/azam-a/6942b485b5d0dcbcd6ae/raw/d79ede4b67f4a118d43cab8c83383d6b31d5e828/characterCounter.py)

### Shortcut

Name | shortcut
-----|---------
Do anything (command palet) | `Ctrl + Shirt + P`
Switch previous / next tab | `ctrl + shift + page_up` \ `ctrl + shift + page_down`
Switch to a specific tab | `ctrl + p`, and write name of your tab (file)
Move a line or a block of line | `ctrl + shift + arrow up` \ `ctrl + shift + arrow down`
Switch upper case | `Ctrl + k` and then `Ctrl + u`
Switch lower case | `Ctrl + k` and then `Ctrl + l`
Sort Lines | `F9` (Edit > Sort Lines)
Goto anywhere | `Ctrl + R`
Open any file | `Ctrl + P`
Goto line number | `ctrl + G`
Spell check | `F6`
New cursor above/below | `alt+shift+arrow`

### Tips / Troubleshooting
#### How to show the menu back?

The easiest way, which works on all platforms, is to use the command pallete: on Windows `ctrl+shift+p` followed by `vmen` (short for View Menu).

### Current settings
Preferences -> Settings
```js
{
    "always_show_minimap_viewport": true,
    "bold_folder_labels": true,
    "color_scheme": "Packages/Materialize/schemes/Material Cobalt.tmTheme",
    "font_options":
    [
        "gray_antialias"
    ],
    "font_size": 12,
    "ignored_packages":
    [
        "Vintage"
    ],
    "indent_guide_options":
    [
        "draw_normal",
        "draw_active"
    ],
    "line_padding_bottom": 3,
    "line_padding_top": 3,
    "overlay_scroll_bars": "enabled",
    "theme": "Material Cobalt.sublime-theme"
}
```

## Vim
how-do-you-paste-with-vim-without-code-being-commented
Before you paste, type this in normal mode:

```
:set paste
```
Then enter insert mode. You will see the status bar say insert (paste). Paste your code. Hit ESC to return to normal mode, and:
```
:set nopaste
```
You are no longer in paste mode.

### Nice my vim
https://gitlab.com/baptiste-dauphin-tool/vimrc#how-to-install-the-awesome-version  
For currenty user
```bash
git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install_awesome_vimrc.sh
```
