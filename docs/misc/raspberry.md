# Raspberry
## Raspbian Release
### Latest
https://www.raspberrypi.org/downloads/raspbian/

### older
Lite images: https://downloads.raspberrypi.org/raspbian_lite/images/  
With desktop: https://downloads.raspberrypi.org/raspbian/images/  
With desktop & recommended software: https://downloads.raspberrypi.org/raspbian_full/images/


## System Installation
### Without monitor nor keyboard
Find where is the location of your __physical__ mm card and __not partition__.  
Example : 
- `mmcblk0`
- ~`mmcblk0p1`~  

And then, burn the unziped image with dd.  `Partitions will be copied because dd is low level copy tool` :)  
Do not forget to sync after dd operation.  
```bash
dmesg -T

sudo \
dd \
bs=1M \
if=/home/baptiste/Downloads/2020-02-13-raspbian-buster-lite.img \
of=/dev/mmcblk0
sync
```

Unplug + plug again mmc card. And mount both fresh partitions.  
I mounted like this.
```
...
mmcblk0                   179:0    0 119.1G  0 disk 
├─mmcblk0p1               179:1    0   256M  0 part /run/media/baptiste/boot
└─mmcblk0p2               179:2    0   1.5G  0 part /run/media/baptiste/rootfs
```

`enable sshd` service by just creating an empty file.
```bash
touch /run/media/baptiste/boot/ssh
```
Fix the ip by editing the commented lines.    
Example : 

Ip | hostname
-|-
192.168.1.100 | pi1

File is owned by root, so edit with sudo or root connected.
```bash
sudo vim /run/media/baptiste/rootfs/etc/dhcpcd.conf
```

- __Unplug the card from your laptop.__
- __Plug it into the raspberry__

### Configure raspbian system

Wait few seconds. You can test if ssh is listening with netcat.
```bash
nc -zvn 192.168.1.100 22
192.168.1.100 22 (ssh) open
```

Use ssh with public key instead of password.
```bash
cat ~/.ssh/id_ed25519.pub | \
ssh pi@192.168.1.100 \
"mkdir -p ~/.ssh && chmod 700 ~/.ssh && cat >>  ~/.ssh/authorized_keys"
```

Now you can directly connect to raspi without prompting password. And `setting system config`
```bash
ssh pi@192.168.1.100

passwd

apt update && apt install -y vim && apt upgrade -y

hostnamectl set-hostname pi1
vi /etc/hosts

reboot
```

## Installing Docker
For Raspbian, installing using the repository is not yet supported. You must instead use the convenience script.
```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker $(whoami)

sudo systemctl enable --now docker
```

## Installing K3S
### Enabling legacy iptables on Raspbian Buster
Raspbian Buster defaults to using nftables instead of iptables. K3S networking features require iptables and do not work with nftables. Follow the steps below to switch configure Buster to use legacy iptables:  
[sources](https://rancher.com/docs/k3s/latest/en/advanced/#enabling-legacy-iptables-on-raspbian-buster)

```bash
sudo iptables -F
sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
sudo reboot
```

### Declare a Master node
```bash
curl -sfL https://get.k3s.io | sh -
```

### Join Workers

`K3S_TOKEN` is stored at `/var/lib/rancher/k3s/server/node-token` on your server node.

```bash
curl -sfL https://get.k3s.io | K3S_URL=https://myserver:6443 K3S_TOKEN=mynodetoken sh -
```
