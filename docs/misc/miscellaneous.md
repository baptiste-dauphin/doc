# Get your public ip into your terminal
```bash
curl -4 icanhazip.com
curl -6 icanhazip.com
```

# Get weather in your terminal
```bash
curl http://v2.wttr.in/Rouen
curl --silent --proxy 10.10.10.10:10000 https://api6.ipify.org\?format\=json
```

# ORLY book generator

https://dev.to/rly

# Why using GitLab and no more GitHub ?
https://www.vox.com/recode/2019/10/9/20906605/github-ice-contract-immigration-ice-dan-friedman

https://www.theverge.com/2019/10/9/20906213/github-ice-microsoft-software-email-contract-immigration-nonprofit-donation

# Jirafeau - file sharing
https://gitlab.com/mojo42/Jirafeau/-/tree/master/

# Microphone noises reduction
[NoiseTorch](https://github.com/lawl/NoiseTorch)

# Terms of Service; Didn’t Read
https://tosdr.org/