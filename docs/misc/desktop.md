# Desktop
## Gnome
GNOME is a free and open-source desktop environment for Unix-like[10] operating systems. GNOME was originally an acronym for GNU Network Object Model Environment, but the acronym was dropped because it no longer reflected the vision of the GNOME project.

### Gnome-keyring
GNOME Keyring is a software application designed to store security credentials such as usernames, passwords, and keys, together with a small amount of relevant metadata. The sensitive data is encrypted and stored in a keyring file in the user's home directory. The default keyring uses the login password for encryption, so users don't need to remember yet another password.  
You can easily access it using the additional tool `seahorse`

Gnome keyring manage
- `SSH keys`
- `GPG keys` (used for files and email encrypting)
- `Passwords keyring` (used to store app and network passwords)
- `Password` (store raw password or secret)
- `Private key` (for certificate)

