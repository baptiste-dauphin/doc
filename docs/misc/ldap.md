# OpenLDAP
## Common command
### Création d'utilisateur
```
dn: cn=jira_ro,ou=services,o=in,c=fr,dc=example,dc=com
changetype: add
objectClass: person
objectClass: inetOrgPerson
sn: jira_ro
cn: jira_ro
description: jira_ro
givenName: jira_ro
mail: jira_ro
o: jira_ro
uid: jira_ro
userPassword: {SSHA}WDmMUA0raKuOBbbZdDA5zXVILllsaepH
```

### Update mdp utilisateur
```
dn: cn=jira_ro,ou=services,o=in,c=fr,dc=example,dc=com
changetype: modify
replace: userPassword
userPassword: {SSHA}WDmMUA0raKuOBbbZdDA5zXVILllsaepV
```

### Suppression d'utilisateur
```
dn: cn=jira_ro,ou=services,o=in,c=fr,dc=example,dc=com
changetype: delete
```

### Ajout de l'utilisateur à un groupe
```
dn: cn=HQReadonly,ou=groups,o=in,c=fr,dc=example,dc=com
changetype: modify
add: member
member: cn=jira_ro,ou=services,o=in,c=fr,dc=example,dc=com
```

### Application dans l'annuaire
```
ldapmodify -H ldaps://myldap.tld -D "DN_WRITE_ACCOUNT" -W -f fichier.ldif
```
L'application est immédiate, pas besoin de restart le service.

### Diagnostics
Vérifier qu'un user peut lister l'ou users:
```
ldapsearch -D "cn=jira_ro,dc=example,dc=com" -W -b "ou=peoples,o=in,c=fr,dc=example,dc=com"
```

## Legacy
```bash
ldapsearch --help
-H URI     LDAP Uniform Resource Identifier(s)
-x         Simple authentication
-W         prompt for bind password
-d 			Debug level
-D binddn  bind DN
-b basedn  base dn for search
SamAccountName SINGLE-VALUE attribute that is the logon name used to support clients and servers from a previous version of Windows.

ldapsearch -H ldap://10.10.10.10 \
-x \
-W \
-D "user@fqdn" \
-b "ou=ou,dc=sub,dc=under,dc=com" "(sAMAccountName=b.dauphin)"
```

```bash
ldapsearch \
-d 1 \
-b "OU=ou1,DC=dc1,DC=under,DC=com" \
-H ldaps://example.org \
-D "CN=b.dauphin,OU=ny,OU=ou1,DC=dc1,DC=dc2,DC=dc3" \
-W 'cn=mysearch'
```

modify an acount (remotly)
```bash
apt install ldap-utils

ldapmodify \
-H ldaps://ldap.company.tld \
-D "cn=b.dauphin,ou=people,c=fr,dc=company,dc=fr" \
-W \
-f b.gates.ldif
```
(.ldif must contains modification data)

#### Locally
```bash
slapcat -f b.gates.ldif
```
#### Generate hash of the password, to update later the password account
will prompt you the string you wanna hash, and generate it in stout
```bash
slappasswd -h {SSHA}
```

#### Content of .ldif
```bash
dn: cn=b.dauphin@github.com,ou=people,c=fr,dc=company,dc=fr
changetype: modify
replace: userPassword
userPassword: {SSHA}0mBz0/OyaZqOqXvzXW8TwE8O/Ve+YmSl
```

