# Hardware
## Memory
### Add memory and make it visible by Debian OS
Even if you add memory with VMWare, debian won't see it `free -m`
You have to make it 'online'
```bash
grep offline /sys/devices/system/memory/*/state | while read line; do echo online > ${line/:*/}; done
```

## Listing
```bash
lspci
```
list you graphic card
```bash
lspci | grep -E "(VGA|3D)" -C 2
lsusb
lscpu : human readable of /proc/cpuinfon
less /proc/cpuinfo
cat /proc/meminfo
nproc
```

## Monitor
Xrandr common cmd
```bash
xrandr --help
xrandr --current

xrandr --output DP-2 --mode 1680x1050 --primary
xrandr --output DP-1 --mode 1280x1024 --right-of DP-2

xrandr --output DP-1 --auto --right-of eDP-1
xrandr --output HDMI-1 --auto --right-of DP-1
```

### Troubleshooting
#### Black screen when screen cast (Wayland)
On `gnome 3.38` the old `X11` display server is not used anymore. It's replaced by `wayland`.  
So you may encounter some screen sharing using.
For `brave` and other `chromium based` browser you can enable `enable-webrtc-pipewire-capturer` flag.  Go to settings and enable it !  
- `chrome://flags`  


[original tuto](https://soyuka.me/make-screen-sharing-wayland-sway-work/)


#### Monitor plugged in but not displaying anything
Case 1 : Reset xrandr config
```bash
xrandr --auto
sudo dpkg-reconfigure libxrandr2
```
Case 2 : Logout of your current Windows Manager (like I3 or cinnamon, or gnome), then select another one. Then logout and go back to your prefered WM. It may resolve the error.

Case 3 : Plug your monitor to another source (another laptop), wait for some pixels to be displayed. Then, plug back to your original laptop.

## Bluetooth
### How to pair AirPods pro on gnome
[Blueman](https://wiki.archlinux.org/index.php/Blueman) is a full featured Bluetooth manager written in GTK. 
Be sure to enable the Bluetooth daemon and start Blueman with `blueman-applet`. A graphical settings panel can be launched with `blueman-manager` or your favourite bluetooth manager. 
```bash
yaourt -S gtk
yaourt -S blueman
```

Then run blueman
```bash
blueman-applet
```

## Cursor
enable right click
if you use Gnome Desktop Manager
```bash
gsettings set org.gnome.desktop.peripherals.touchpad click-method areas
```
get your devices details
libinput list-devices
