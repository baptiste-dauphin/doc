# Dev friendly

A developper shouldn't think about all the `underlying infra` system.  The maximum infrastructure effort should be in which environment his app will run. `bar-system`, `docker` direct access or `kubernetes` ecosystem.

## Jupyter
https://jupyter.org/

### BinderHub
[binderhub](https://github.com/jupyterhub/binderhub)

BinderHub allows you to `BUILD` and `REGISTER` a Docker image from a Git repository, then `CONNECT` with __JupyterHub__, allowing you to create a public IP address that allows users to interact with the code and environment within a live JupyterHub instance. You can select a specific branch name, commit, or tag to serve.