# Terminal
## Pimp my terminal
[Source](https://hackernoon.com/how-to-trick-out-terminal-287c0e93fce0)
gnome-terminal
GNOME Terminal (the default Ubuntu terminal): `Open Terminal` → `Preferences` and click on the selected profile under `Profiles`. Check Custom font under Text Appearance and select `MesloLGS NF Regular` or `Hack` or the font you like.

### Debian
1- Ensure that your terminal is `gnome-terminal`
```bash
update-alternatives --get-selections | grep -i term
x-terminal-emulator            manual   /usr/bin/gnome-terminal.wrapper
```

#### Graphicaly
Install  `dconf`
```bash
sudo apt-get install dconf-tools
dconf-editor
```
Run it and go to path `org` > `gnome` > `desktop` > `interface` > `monospace-font-name`

CLI
gsettings offers a simple commandline interface to GSettings. It lets you get, set or monitor an individual key for changes.  
__To *Know* current settings type following commands in terminal :__
```bash
gsettings get org.gnome.desktop.interface document-font-name
gsettings get org.gnome.desktop.interface font-name 
gsettings get org.gnome.desktop.interface monospace-font-name
gsettings get org.gnome.nautilus.desktop font
```
__You can *set* fonts by following commands in terminal :__
For example `Monospace 11` do not support symbol. Which is uggly if you have a custom shell.  
My choises which differs from default :  
The last __number argument__ is the size
> for terminal
```bash
gsettings set org.gnome.desktop.interface monospace-font-name 'Hack 12'
```
> for soft like Keepass2
```bash
gsettings set org.gnome.desktop.interface font-name 'Hack 12'
```
__Get list of available fonts__
```bash
fc-list | more
fc-list | grep -i "word"
fc-list | grep -i UbuntuMono
```
To lists font faces that cover Hindi language:
```bash
fc-list :lang=hi
```
search by family
```bash
fc-list  :family="NotoSansMono Nerd Font Mono"
```
search with complete name
```bash
fc-list  :fullname="Noto Sans Mono ExtraCondensed ExtraBold Nerd Font Complete Mono"
```

To find all similar keys on schema type following command:
```bash
gsettings list-recursively org.gnome.desktop.interface
```
To reset all valuses of keys run following command in terminal:
```bash
gsettings reset-recursively org.gnome.desktop.interface
```

## xdg-settings / update-alternatives
In some case, __update-alternatives__ is not enough. Especially for __Url Handling or web browsing__  
`xdg-settings - get various settings from the desktop environment`

### Set brave as default browser
In my use case, I set up update-alternatives but it didn't change the behaviour for URL handling (printed in my terminal especially useful after `git push` for creating a merge request).  
Correctly setup but doesn't affect behaviour
```
sudo update-alternatives --config x-www-browser
```

```bash
xdg-settings check default-web-browser brave.desktop
no

xdg-settings --list default-url-scheme-handler
Known properties:
  default-url-scheme-handler    Default handler for URL scheme
  default-web-browser           Default web browser


xdg-settings get default-web-browser
firefox-esr.desktop
```

If `xdg` or `xdg-mime` still dont' work you can manually edit.
```bash
cat ~/.config/mimeapps.list

[Default Applications]
x-scheme-handler/http=brave.desktop
x-scheme-handler/https=brave.desktop
x-scheme-handler/ftp=brave.desktop
x-scheme-handler/chrome=brave.desktop
text/html=brave.desktop
application/x-extension-htm=brave.desktop
application/x-extension-html=brave.desktop
application/x-extension-shtml=brave.desktop
application/xhtml+xml=brave.desktop
application/x-extension-xhtml=brave.desktop
application/x-extension-xht=brave.desktop
```