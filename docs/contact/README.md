# Contact
- [baptistedauphin@protonmail.com](mailto:baptistedauphin@protonmail.com)  
- [baptistedauphin76@gmail.com](mailto:baptistedauphin76@gmail.com)  
- pgp : `5A04 0187 EDDD D936 B41E 8268 E457 7920 E027 46B3`  

---
- [www.baptiste-dauphin.com](http://www.baptiste-dauphin.com/)  
- [cv.baptiste-dauphin.com](http://cv.baptiste-dauphin.com/)  
- [blog.baptiste-dauphin.com](http://blog.baptiste-dauphin.com/)  

---
- [GitLab](https://gitlab.com/baptiste-dauphin/)  
- [LinkedIn](https://www.linkedin.com/in/baptiste-dauphin/)  
