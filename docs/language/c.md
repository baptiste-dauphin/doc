# C
## Pourquoi le C ?

Avantages
- __Robuste__. Existe depuis 1972, le C a été confronté à un très grand nombre de scénarios. Les bug on eu le temps d'être corrigés. Et de manière général, d'être optimisé.
- __Rapide__. Le C est un language humain (et non binaire), il a besoin d'être compilé (par un __compilateur__) pour être rendu interpretable pour la machine. Il est compilé une fois pour toute, à la différence des langages __interprétés__ qui necessitent un interpreteur pour "convertir" en permanence à la volé les instructions humaines vers la machine, ce qui ajoute un temps de traitement supplémentaire. Le C s'abstrait de cette couche d'interpretation.

Inconvénients
- __Les erreurs peuvent se produire rapidement__ : En C, c'est le code écrit par le developpeur qui defini les plages de mémoire utilisée lors de la création et lors de la libération. Une gestion humaine induit très facilement des oublis ou des décalages. Les langages interprétés comme le __Python__ ou le __Java__ ont un gestionaire de mémoire dans leur interpreteur (garbage collector). Gestionnaire qui a été testé et corrigé dans beaucoup de scénarios par beaucoup de personnes à l'invers de la gestion manuelle dans le code du programme directement qu'impose le C.

