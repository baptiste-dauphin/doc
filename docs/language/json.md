# Json
```bash
cat README.md | jq .
cat README.md | jq '.[].id'
```

Get a single scalar values + (different form, as a pipeline)
```bash
cat s1.json | jq ' .spec.replicas '
cat s1.json | jq ' .spec | .replicas '
```

Get two scalar values
```bash
cat s1.json | jq ' .spec.replicas, .kind '
```

Get two scalar values and concatenate/format them into a single string
```bash
cat s1.json | jq ' "replicas: " + (.spec.replicas | tostring) + " kind: " + .kind '
```

Select an object from an array of object based on one of the names
```bash
cat dep.json | jq ' .status.conditions | map(select(.type == "Progressing")) '
```


## Extract, concatenate, select where key=value
[Source](https://mosermichael.github.io/jq-illustrated/dir/content.html)