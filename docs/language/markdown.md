# Markdown
[Highlight syntax](https://support.codebasehq.com/articles/tips-tricks/syntax-highlighting-in-markdown)

[Master Markdown tutorial](https://guides.github.com/features/mastering-markdown/)

## VuePress
### Custom containers
There is some additional feature like :

::: tip
This is a tip
:::

::: warning
This is a warning
:::

::: danger
This is a dangerous warning
:::

::: details
This is a details block, which does not work in IE / Edge
:::

#### Click me to view

::: danger STOP
Danger zone, do not proceed
:::

::: details Click me to view the code
```js
console.log('Hello, VuePress!')
```
:::


[VuePress doc](https://vuepress.github.io/reference/default-theme/markdown.html#markdown)

## Centered Text And Images
```md
Normal Text

<p align="center">
  <b>Some Links:</b><br>
  <a href="#">Link 1</a> |
  <a href="#">Link 2</a> |
  <a href="#">Link 3</a>
  <br><br>
  <img src="http://s.4cdn.org/image/title/105.gif">
</p>

Normal text
```
[Result](https://gist.github.com/ProjectCleverWeb/4052a53abd8c6f8c6aea)
