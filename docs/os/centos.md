# CentOS
CentOS specific commands which differs from debian
## Iptables
```bash
iptables-save > /etc/sysconfig/iptables
```
## OS Version
```bash
cat /etc/system-release
CentOS Linux release 7.6.1810 (Core)
```
## Yum
```bash
yum install httpd
yum remove  postgresql.x86_64
yum update postgresql.x86_64
yum search firefox
yum info samba-common.i686

yum groupinstall 'DNS Name Server'

yum repolist

yum check-update

yum list | less
yum list installed | less
yum provides /etc/sysconfig/nf
yum grouplist

yum list installed | grep unzip
```
`to be updated...`