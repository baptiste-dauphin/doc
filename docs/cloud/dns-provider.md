# DNS Provider
## Gandi
* [Generate your API Key](https://doc.livedns.gandi.net/#id5)  
[Official documentation](https://doc.livedns.gandi.net/)
* Get your company id (i.e. __sharing_id__)
```bash
curl -H"Authorization: Apikey $APIKEY" \
  "https://api.gandi.net/v5/organization/organizations\?type=company" \
  | jq '.[].id'
```

#### Back up domains
List all domains
```bash
curl -H"X-Api-Key: $APIKEY" \
  https://dns.api.gandi.net/api/v5/domains\?sharing_id\=$SHARING_ID \
  | jq -r '.[].fqdn' \
  > domain.list
```

Copy data  
For each records in a given domain get all records info (type, ttl, name, href, values) and create.
```bash
mkdir domains_records

while read domain; do
  (curl -H"X-Api-Key: $APIKEY" \
    https://dns.api.gandi.net/api/v5/domains/$domain/records\?sharing_id\=$SHARING_ID \
    | jq . > ./domains_records/$domain) &
done <domain.list
```

## Ovh
### API
Example : You want to use __Let's encrypt certbot wildcard domain certificate generation + auto renewal__  
The [certbot doc](https://certbot-dns-ovh.readthedocs.io/en/stable/#credentials) ask you to have the following acces to the OVH api (if your DNS provider is ovh obviously, it depends)

- `GET /domain/zone/*`
- `PUT /domain/zone/*`
- `POST /domain/zone/*`
- `DELETE /domain/zone/*`

Then, you need [3 credetntials](https://certbot-dns-ovh.readthedocs.io/en/stable/#credentials-ini)  
Example credentials file:
```
# OVH API credentials used by Certbot
dns_ovh_endpoint = ovh-eu
dns_ovh_application_key = MDAwMDAwMDAwMDAw
dns_ovh_application_secret = MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAw
dns_ovh_consumer_key = MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAw
```

1. Creation of your __application keys__  (1/3 & 2/3)

Authentication consists of two keys. The first is the application key. Any application wanting to communicate with the OVH API must be declared in advance.

Click on the following link: https://eu.api.ovh.com/createApp/, enter your customer ID, your password, and the name of your application. The name will be useful later if you want to allow others to use it.

You get two keys:

the __application key__
```
7kbG7Bk7S9Nt7ZSV
```
your __secret application key__
```
EXEgWIz07P0HYwtQDs7cNIqCiQaWSuHF
```

2. Get your __consumer key__ (3/3)
```bash
curl -XPOST -H"X-Ovh-Application: 7kbG7Bk7S9Nt7ZSV" -H "Content-type: application/json" \
https://eu.api.ovh.com/1.0/auth/credential  -d '{
  "accessRules": [
    {
      "method": "DELETE",
      "path": "/domain/zone/*"
    },
    {
      "method": "GET",
      "path": "/domain/zone/*"
    },
    {
      "method": "POST",
      "path": "/domain/zone/*"
    },
    {
      "method": "PUT",
      "path": "/domain/zone/*"
    }
  ],
  "redirection": null
}' | \
jq
```
```
{
  "validationUrl": "https://eu.api.ovh.com/auth/?credentialToken=iQ1joJE0OmSPlUAoSw1IvAPWDeaD87ZM64HEDvYq77IKIxr4bIu6fU8OtrPQEeRh",
  "consumerKey": "MtSwSrPpNjqfVSmJhLbPyr2i45lSwPU1",
  "state": "pendingValidation"
}
```
[Doc of this endpoint](https://api.ovh.com/console/#/auth/credential#POST)

3. Validate your __consumer key__  
Connect the authentication token to an OVH customer account
In the response, you will be sent a validation URL and a consumerKey (the token, named CK). This token is not initially linked to any customer. You (or another customer) will connect your OVH account to this token by logging in using the URL.

This stage will enable you to identify any OVH customer and to obtain rights on their account. This could be useful if you want to develop an app for the community. Otherwise you can log in directly on the page.

Go here (example) : https://eu.api.ovh.com/auth/?credentialToken=iQ1joJE0OmSPlUAoSw1IvAPWDeaD87ZM64HEDvYq77IKIxr4bIu6fU8OtrPQEeRh
