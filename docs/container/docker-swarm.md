# Docker Swarm
## Node
print cluster nodes
```bash
docker node ls
```

get address + role
```bash
for node in $(docker node ls -q); do     docker node inspect --format '{{.Status.Addr}} ({{.Spec.Role}})' $node; done
```

Print labels of nodes
```bash
docker node ls -q | xargs docker node inspect \
  -f '[{{ .Description.Hostname }}]: {{ range $k, $v := .Spec.Labels }}{{ $k }}={{ $v }} {{end}}'
```

## Join new node
swarm manager shell
```bash
docker swarm join-token worker
```
We output a copy pastable bash line, like the following ! (Be carefull it doesn't include listen ip of the worker)

new worker shell

```bash
docker swarm join \
  --token <TOKEN_WORKER> \
  --listen-addr WORKER-LISTEN-IP:2377 \
   <MANGER-LISTEN-IP>:2377
```

## Service
On swarm __manager__

### Print services for a given name
```bash
docker service ls --filter name=streaming --format '{{.Name}}'
docker service ls -f "desired-state=running" --format '{{.Name}}'

```

### Info of service
```bash
docker service ps api-streaming
```

### Filter on running services
```bash
docker service ps -f "desired-state=running" 869_api-games
```

### find where (NODE) an app is running.
```bash
docker service ps api-streaming --format '{{.Node}}'
node04
```


### Last time updated
```bash
docker service inspect log-master_logstash -f '{{ json .UpdatedAt }}'
```
