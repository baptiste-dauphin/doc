#!/bin/bash
# set -euo pipefail

DIR="$(dirname $(realpath "$0"))"
# Laptop screen-code
monitor1="eDP-1"

function base_command() {
	$DIR/keyboard-fr.sh
	$DIR/ssh_agent.sh
}

function monitor_config_home() {
	monitor2="HDMI-1"
	xrandr --output $monitor2 --auto --right-of $monitor1
}
function monitor_config_office() {
	monitor2="DP-1"
	monitor3="HDMI-1"
	xrandr --output $monitor2 --auto --right-of $monitor1
	xrandr --output $monitor3 --auto --right-of $monitor2
}

function print_help(){
	echo "usage : "
	echo "./startup --home"
	echo "or"
	echo "./startup --office"
}

# execution
if [[ "$#" -eq 0 ]]; then
	print_help
	exit 1
fi

case $1 in
	--maison|--home) 	base_command; monitor_config_home
	;;
	-o|--office) 		base_command; monitor_config_office
	;;
	-h|--help)  		print_help
	;;
	*) 					echo "Unknown parameter passed: $1" ; print_help ; exit 1
	;;
esac;
