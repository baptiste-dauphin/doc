# Script usage
## vault-unix-root

A tool to clip VM root password from vault API based on Xorg window name

When you are on ssh the remote VM name is display on the title of the window, this script use X tools to retreive it and make
a vault call to retreive apropriate root password and set it in your local clipboard (click scroll wheel button).

### requirements

You have to install following paquage:
- vault : HashiCorp cli which is available here https://www.vaultproject.io/downloads.html
- xprop
- xdotool
- notify-send (you could simply remove these commands in script) (included in debian package libnotify-bin)
- xclip (included in debian package xclip)

Replace "i3-sensible-terminal" with your how terminal.

If your unix username differ from you LDAP (AD) account name you have to specify it in vault-unix-root.sh script (variable USER)

Adjust the regex to retreive VMname variable and check window name.

### Installation

1. Place vault-unix-root.sh somewhere in (maybe rename it) with icons folders.
2. Setup a keyboard shortcut to run vault-unix-root.sh script

