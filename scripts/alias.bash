## cd
alias cdinfra='cd $company_git_dir_path'
alias cdoverlord='cdinfra && cd $overlord_dir'
alias cdansible='cdinfra && cd ansible'
alias cdmyinfra='cd $personal_git_dir_path/infra'
alias cdoc='cd $personal_git_dir_path/doc'
alias cdk8s='cdinfra && cd k8s-deployment'
alias cdhelm='cdinfra && cd helm'
alias cdkb='cdinfra && cd kb'
alias cdmind='cd $personal_git_dir_path/mind'
alias cdblog='cd $personal_git_dir_path/blog'
alias cdcv='cd $personal_git_dir_path/cv'
alias aliasedit='${=EDITOR_NO_WAIT} $personal_git_dir_path/doc/scripts/alias.bash'

## backup
alias i3config-backup='cat ~/.config/i3/config > $personal_git_dir_path/doc/dot_files/i3_config'
alias zshrc-check-diff="diff -u $personal_git_dir_path/doc/dot_files/.zshrc ~/.zshrc"
alias zshrc-backup-to-git='cat ~/.zshrc > $personal_git_dir_path/doc/dot_files/.zshrc'
alias zshrc-restore-from-git='cat $personal_git_dir_path/doc/dot_files/.zshrc > ~/.zshrc'

## config 
alias sshconfig='${=EDITOR_NO_WAIT} ~/.ssh/config'

## git

gcmsgi() {
    git commit -m "$(git_current_branch): $*"
}

# gcmsg() {
#     git commit -m "$*"
# }

## misc
alias hashtag="echo \"grep -iv '^\s*[#\;]\|^\s*$'\""
alias working-sheet-of-the-day='new_working_sheet_of_the_the_day ~/Documents/working_sheet_of_the_day'
alias report-of-the-day='new_working_sheet_of_the_the_day ~/Documents/report_of_the_day'
alias weekly-report='new_working_sheet_of_the_the_day ~/Documents/weekly_report'
alias i3config='${=EDITOR_NO_WAIT} ~/.config/i3/config'
