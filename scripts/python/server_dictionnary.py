#!/usr/bin/python

# manual test usage to test JSON output
# ./server_dictionnary.py.py | jq .

import json
data = [
        {'{#SERVER_NAME}': 'share.baptiste-dauphin.com', '{#PORT}': "443"},
        {'{#SERVER_NAME}': 'analytics.baptiste-dauphin.com', '{#PORT}': "443"},
        {'{#SERVER_NAME}': 'no-full-chain.baptiste-dauphin.com', '{#PORT}': "443"},
        {'{#SERVER_NAME}': 'full-chain.baptiste-dauphin.com', '{#PORT}': "443"}
        ]

# exemple let here for futur data explorations in deepth
# element = {'{#NODENAME}': 'name',
#            '{#NODETYPE}': node['type']}

# get json string from that array
json=json.dumps({'data': data})
print json
