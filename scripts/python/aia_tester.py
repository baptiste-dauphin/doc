#!/usr/bin/python2

import sys
import pycurl
from StringIO import StringIO

# /usr/bin/python2 ./aia_tester.py https://full-chain.baptiste-dauphin.com
# /usr/bin/python2 ./aia_tester.py https://no-full-chain.baptiste-dauphin.com

url = sys.argv[1]
verbose = False

buffer = StringIO()
c = pycurl.Curl()
c.setopt(pycurl.URL, url)
c.setopt(pycurl.WRITEDATA, buffer)

try:
    c.perform()
    c.close()
    print('succeed')
except pycurl.error as err:
    if verbose:
        print('failure %s: %s',
              type(err).__name__, err)
    else:
        print('failure')
    exit(1)
