#!/bin/bash

DIR=$1
FILE_OF_THE_DAY_LOCATION="$DIR/$(date +%Y_%m_%d.md)"

if [ "$#" -ne 1 ]
then
    echo "Illegal number of parameters"
    echo "you have to specify a directory where store working sheet !!!"
    echo "example : new_working_sheet_of_the_the_day ~/Documents/working_sheet_of_the_day"
    exit 1
else
	if [[ ! -d "$DIR" ]]; then
		echo "Directory $DIR doesn't exist. Creation..."
		mkdir -p "$DIR"
	fi
	if [[ -e $FILE_OF_THE_DAY_LOCATION ]]
	then
		echo "file of the day $FILE_OF_THE_DAY_LOCATION already exists"
		echo "Opening document..."
		$EDITOR_NO_WAIT $FILE_OF_THE_DAY_LOCATION
		exit 2
	else 
		echo "File $FILE_OF_THE_DAY_LOCATION doesn't exist. Creation of it !"
		touch $FILE_OF_THE_DAY_LOCATION
		echo "Opening document..."
		$EDITOR_NO_WAIT $FILE_OF_THE_DAY_LOCATION
		exit 0
	fi
fi
