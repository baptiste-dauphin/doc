#!/bin/bash
# set -euo pipefail

# execution
echo -n "What is the type of this work station ? [ personal / professional ]: "
read ans
echo $ans

case $ans in
	personal) 			prefix="~/Git"
	;;
	professional) 		prefix="~/Git/personal"
	;;
	*) 					echo "Unknown parameter passed: $ans" ;
	;;
esac;

repo_name='doc'
full_path=$prefix/$repo_name
# converting into real full path
full_path=$(eval echo $full_path)

function clone_repo(){
	if [[ ! -d $full_path ]]; then
		git clone https://gitlab.com/baptiste-dauphin/doc $full_path
	else 
		echo 'Folder : ' "$full_path" 'already exists'
	fi
}

function backup_zshrc(){
	echo 'Try to back up your zshrc (~/.zshrc)...'
	cp ~/.zshrc ~/.zshrc.backup.$(date +%d-%m-%Y-%H:%M:%S)
	if [[ $? -eq 0 ]]; then
		echo 'Back up success'
		echo "~/.zshrc.backup.$(date +%d-%m-%Y-%H:%M:%S)"
	else
		echo 'Backup failed !!!';
	fi

}

function install_zshrc(){
	echo 'Try to install zshrc from repo...'
	/bin/cp -f $full_path/dot_files/.zshrc ~/.zshrc --backup
	if [[ $? -eq 0 ]]; then
		echo 'Installation success'
	else
		echo 'Backup failed !!!';
	fi
}

function refresh_shell(){
	echo 'Refreshing your shell...'
	echo 'exec $SHELL...'
	exec $SHELL
}


## execution
clone_repo
backup_zshrc
install_zshrc
refresh_shell