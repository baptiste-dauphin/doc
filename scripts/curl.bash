#!/bin/bash

nocache=0
query_type="web"
user_agent="toto"

show_help() {
	echo -e "API tester"
	echo -e ""
	echo -e "Usage:"
	echo -e "-h\tShow this help"
	echo -e "-i\tIP address of an API endpoint"
	echo -e "-n\tSet nocache value"
	echo -e "-q\tQuery"
	echo -e "-t\tSet query type (default: web)"
}

# Reset getopts
OPTIND=1

while getopts "h?i:n:q:t:u:c:" opt
do
    case "${opt}" in
    h|\?)
        show_help
        exit 0
        ;;
    n)  nocache=${OPTARG}
        ;;
    i)  ip=${OPTARG}
        ;;
    q)  query=${OPTARG}
        ;;
    t)  query_type=${OPTARG}
        ;;
    u)  user_agent=${OPTARG}
        ;;
    esac
done

shift $((OPTIND-1))
[ "${1:-}" = "--" ] && shift


if [[ "${nocache}" == 0 ]]; then
	nocache=""
else
	nocache="&__nocache=${nocache}"
fi

curl "http://${ip}/api/search/${query_type}?count=10&q=${query}&t=${query_type}&device=desktop&safesearch=1&locale=fr_FR&uiv=4${nocache}" \
	-H "User-Agent: ${user_agent}" \
	-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' \
	-H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed \
	-H 'DNT: 1' \
	-H 'Connection: keep-alive' \
	-H 'Upgrade-Insecure-Requests: 1' \
	-H 'Pragma: no-cache' \
	-H 'Cache-Control: no-cache' \
	-H 'TE: Trailers' \
	-H "Host: example.com"
