#!/bin/bash
# set -euo pipefail

function print_help(){
	echo 'usage : '
	echo './cn-info-extractor.bash --fqdn-list list.txt'
	echo './cn-info-extractor.bash --fqdn example.org'
	echo "./cn-info-extractor.bash --fqdn '*.example.org'"
	echo 'PORT=993                                            ./cn-info-extractor.bash --fqdn example.org'
	echo 'PORT=993 TIMEOUT=2                                  ./cn-info-extractor.bash --fqdn example.org'
	echo 'PORT=993 TIMEOUT=2 WILDCARD_REPLACE_CHARS=aaaaaaaaa ./cn-info-extractor.bash --fqdn example.org'
}

function openssl_s_client_text_output(){
	echo | \
	timeout $timeout openssl s_client \
	-servername $1 \
	-connect $1:$port \
	2>/dev/null | \
	openssl x509 -text
}


function get_expiration_date(){
	openssl_s_client_text_output $1 | grep -i 'After' |  perl -pe 's/^\s+//'
}

function get_san(){
	openssl_s_client_text_output $1 | grep -i 'DNS:' |  perl -pe 's/^\s+//'
}

function get_data_and_san(){
	cn=$1
	sanitized_cn="${1/\*/$wildcard_replace_chars}"

	echo -e "==========================================="
	echo -e "Found  CN : "$cn""
	echo -e "Tested CN : "$sanitized_cn":$port\n"
	get_expiration_date $sanitized_cn
	echo ""
	get_san $sanitized_cn
	echo ""
}

function display_parameters(){
	echo '==========================================='
	echo 'Parameters :'
	echo '- timeout : ' $timeout
	echo '- port    : ' $port
	echo '- wildcard_replace_chars : ' $wildcard_replace_chars
	echo '==========================================='
}

function check_args(){
	if [[ "$1" -ne 2 ]]; then
		print_help
		exit 1
	fi
}

default_timeout='10'
default_port='443'
default_wildcard_replace_chars='ooooooooooooooo'
wildcard_replace_chars='ooooooooooooooo'

input_type=$1
timeout=${TIMEOUT:-$default_timeout}
port=${PORT:-$default_port}
wildcard_replace_chars=${WILDCARD_REPLACE_CHARS:-$default_wildcard_replace_chars}


check_args $#
display_parameters

# RUN
case $input_type in
	--fqdn-list)
		while read cn ; do
			get_data_and_san $cn
		done < $2
	;;
	--fqdn)
		get_data_and_san $2
	;;
	-h|--help)
		print_help
	;;
	*)
		echo "Unknown parameter passed: $1"
		print_help
		exit 1
	;;
esac;
