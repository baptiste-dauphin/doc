#!/bin/bash

# Usage: ./crack-keepass.sh passwords.kdbx dict.txt
# 
# The dictionary file can be generated with:
# https://github.com/TimurKiyivinski/permutatify

list_of_char="_ #"
while read i
do
    echo toto
    for (( a=0; a<${#list_of_char}; a++ )); do
        for (( n = 0; n < 100; n++ )); do
            echo "====================================="
            password="${i}${list_of_char:a:1}${n}"
            echo "Using password: $password"

            echo "$password" | keepassxc-cli ls $1 -q
            
            if [[ $? -eq 0 ]]; then
                echo "###################"
                echo "####  FOUND !!! ###"
                echo "###################"
                echo "Keepass entries : "
                echo "$password" | keepassxc-cli show $password $(echo "$password" | keepassxc-cli ls $password -q) -q
                exit 0
            else
                echo "password: $password was incorrect"
            fi
        done
    done
done < $2


# for (( a=0; a<${list_of_char}; a++ )); do
#     echo "${list_of_char:a:1}"
# done