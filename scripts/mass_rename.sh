#!/bin/sh
# Rename *.bash files into *.md
for file in *.bash; do
  mv "$file" "${file%.bash}.md"
done