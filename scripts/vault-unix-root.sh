#!/bin/bash

USER=${USER:LDAPusername}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR
progname=$(basename "$0")
# You should have to be authenticated:
# vault login -method=ldap username=${USER}
# VAULT_ADDR might be loaded into in ~/.zshrc
# or within this script
# export VAULT_ADDR="https://vault.example.com:9999/"
currentdate=$(date +%FT%T.%N)
expiredate=$(vault token lookup |grep expire_time |awk '{print $2}')
win_name=$(xprop -id $(xdotool getactivewindow) | grep _NET_WM_NAME | cut -d'"' -f2)

if [[ ! -f ~/.vault-token ]]; then
  i3-sensible-terminal -- vault login -method=ldap username=${USER};
  notify-send -i $DIR/icons/error-icon.png "${progname}: Token missing." "Execute vault login command."
  exit 1
fi

if [[ $currentdate > $expiredate ]]; then
  i3-sensible-terminal -- vault login -method=ldap username=${USER};
  notify-send -i $DIR/icons/error-icon.png "${progname}: Token expired." "Execute vault login command"
  exit 2
fi

echo $win_name | grep '^'${USER}'@.*:.*$' &>/dev/null
if [[ $? -ne 0 ]]; then
  notify-send -i $DIR/icons/error-icon.png "${progname}: ${win_name}" "Invalid window name: Cannot extract VM hostname."
  exit 3
fi

VMname=$(echo $win_name | sed 's/^'${USER}'@\(.*\):.*$/\1/g')
path=$(vault kv get path/to/secret | tail -n 1 | awk '{print $2}')
# todo check pss is empty ?
if [ -z "$pass" ]; then
  notify-send -i $DIR/icons/error-icon.png "${progname}: ${VMname}" "Not found in vault db: ${VAULT_ADDR}"
  exit 4
fi
echo $pass | xclip
notify-send -i $DIR/icons/success-icon.png "${progname}: ${VMname}" 'Root pwd available in clipboard !'
