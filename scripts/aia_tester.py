#!/usr/bin/python2

import sys
import pycurl
from StringIO import StringIO

# /usr/bin/python2 ./aia_tester.py https://full-chain.baptiste-dauphin.com
# /usr/bin/python2 ./aia_tester.py https://no-full-chain.baptiste-dauphin.com

url = sys.argv[1]

buffer = StringIO()
c = pycurl.Curl()
c.setopt(pycurl.URL, url)
c.setopt(pycurl.WRITEDATA, buffer)

try:
    c.perform()
    c.close()
    print('secured connection SUCCEED')
except pycurl.error as err:
    print('secured connection FAILED! %s: %s',
        type(err).__name__, err)
    exit(1)
