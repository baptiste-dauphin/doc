[![pipeline status](https://gitlab.com/baptiste-dauphin/doc/badges/master/pipeline.svg)](https://gitlab.com/baptiste-dauphin/doc/-/commits/master)

# My shell confort
## Prerequisites
### Git repo clone structure
One folder per usage :
- Personal
- Professional

i.e.
```bash
tree $HOME/Git -L 1

/home/baptiste/Git
├── company
├── personal
└── user.values
```
```bash
mkdir ~/Git/personal
git clone git@gitlab.com:baptiste-dauphin/doc.git ~/Git/personal/doc
cd ~/Git/personal/doc

sudo apt install yarn libatomic1 libgconf-2-4 apt-transport-https curl wget keepassxc tmux zsh-syntax-highlighting keychain

# install zsh syntax highlights
sudo apt install zsh-syntax-highlighting

# install fonts
sudo apt-get install fonts-powerline

# clone dircolors-solarized (to be loaded by ~/.zshrc)
git clone https://github.com/seebi/dircolors-solarized.git ~/Git/dircolors-solarized

# install powerlevel10k
https://github.com/romkatv/powerlevel10k#get-started

# copy customized zsh config
cp dot_files/.zshrc ~/.zshrc

# Feed files
vim /home/baptiste/Git/user.values
```

### User values
```bash
vim ~/Git/user.values
```
```bash
personal_git_dir_path=~/Git/personal
company_git_dir_path=~/Git/company
overlord_dir='************'
vault_addr='https://**********/'
editor='subl -n -w'
editor_no_wait='subl'
```

## Installation
### Zsh
- [zsh](https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH) shell
### Oh-my-zsh (zsh customization)
- [`oh-my-zsh`](https://github.com/ohmyzsh/ohmyzsh)

---

# Resources
Miscellaneous
- [:open_file_folder: Scripts](scripts)
